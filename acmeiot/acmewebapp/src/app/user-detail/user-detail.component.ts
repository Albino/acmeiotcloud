import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { User, Sensor } from '../data.model';
import { DataService } from '../data.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: [ './user-detail.component.css' ]
})
export class UserDetailComponent implements OnInit {
  @Input() user: User;

  sensors: Sensor[];

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getUser().subscribe(user => {
      this.user = user;
      this.getSensors();
    });
  }

  getUser(): Observable<User> {
    const id = this.route.snapshot.paramMap.get('userid');
    return this.dataService.getUser(id);
  }

  goBack(): void {
    this.location.back();
  }

  seeAllUserNotifications(): void {
    this.router.navigateByUrl(`/notifications/${this.user._id}`);
  }

  addSensor(sensorName: string): void {
    const sensor = {
      name: sensorName,
      userId: this.user._id
    };

    this.dataService.addSensor(sensor)
      .subscribe( (s: Sensor) => this.sensors.push(s) );
  }

  getSensors(): void {
    this.dataService.getAllUsersSensors(this.user._id)
      .subscribe( (d: Sensor[]) => this.sensors = d);
  }
}
