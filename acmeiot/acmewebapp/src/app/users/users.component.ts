import { Component, OnInit } from '@angular/core';
import { User } from '../data.model';
import { DataService } from '../data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.dataService.getUsers()
    .subscribe(users => this.users = users);
  }

  add(username: string): void {
    username = username.trim();
    if (!username) { return; }
    this.dataService.addUser({ name: username })
      .subscribe(user => {
        this.getUsers();
        console.log(user);
      });
  }

  delete(user: User): void {
    this.users = this.users.filter(h => h !== user);
    this.dataService.deleteUser(user).subscribe();
  }
}
