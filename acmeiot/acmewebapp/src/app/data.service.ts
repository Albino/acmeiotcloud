import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { User, Sensor, Notification } from './data.model';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class DataService {

  private baseApi = 'api/';
  private usersApi = this.baseApi + 'users';
  private sensorsApi = this.baseApi + 'sensors';
  private notificationsApi = this.baseApi + 'notifications';

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET users from the server */
  getUsers (): Observable<User[]> {
    return this.http.get<User[]>(this.usersApi)
      .pipe(
        tap(users => this.log(`fetched users`)),
        catchError(this.handleError('getUsers', []))
      );
  }

  /** GET user by id. Return `undefined` when id not found */
  getUserNo404<Data>(id: string): Observable<User> {
    const url = `${this.usersApi}?q={ "_id" : "${id}" }`;
    return this.http.get<User[]>(url)
      .pipe(
        map(users => users[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} user id=${id}`);
        }),
        catchError(this.handleError<User>(`getUser id=${id}`))
      );
  }

  /** GET user by id. Will 404 if id not found */
  getUser(id: string): Observable<User> {
    const url = `${this.usersApi}/${id}`;
    return this.http.get<User>(url).pipe(
      tap(_ => this.log(`fetched user id=${id}`)),
      catchError(this.handleError<User>(`getUser id=${id}`))
    );
  }

  /* GET users whose name contains search term */
  searchUsers(term: string): Observable<User[]> {
    if (!term.trim()) {
      // if not search term, return empty user array.
      return of([]);
    }
    return this.http.get<User[]>(`api/users?q={ "name":"${term}"}`).pipe(
      tap(_ => this.log(`found users matching "${term}"`)),
      catchError(this.handleError<User[]>('searchUsers', []))
    );
  }

  getAllUsersSensors(userId: string): Observable<Sensor[]> {

    const url = `${this.sensorsApi}?q={ "userId" : "${userId}" }`;
    this.log(url);
    return this.http.get<Sensor[]>(url).pipe(
      tap(_ => this.log(`found sensors owned by the user ${userId}`)),
      catchError(this.handleError<Sensor[]>('getAllUsersSensors'))
    );
  }

  /** GET: Notifications */
  getNotifications(userId: string, sensorId: string): Observable<Notification[]> {
    let url: string;
    if (sensorId) {
      url = `${this.notificationsApi}?q={ "userId" : "${userId}" , "sensorId" : "${sensorId}" }`;
    } else {
      url = `${this.notificationsApi}?q={ "userId" : "${userId}" }`;
    }

    return this.http.get<Notification[]>(url).pipe(
      tap(_ => this.log(`found notifications for the user ${userId}`)),
      catchError(this.handleError<Notification[]>('getAllUsersNotifications'))
    );
  }

  //////// Save methods //////////

  /** POST: add a new user to the server */
  addUser (user: any): Observable<User> {
    return this.http.post<any>(this.usersApi, user, httpOptions).pipe(
      tap((u: any) => this.log(`added user w/ id=${u._id}`)),
      catchError(this.handleError<User>('addUser'))
    );
  }

  /** DELETE: delete the user from the server */
  deleteUser (user: User | string): Observable<User> {
    const id = typeof user === 'string' ? user : user._id;
    const url = `${this.usersApi}/${id}`;

    return this.http.delete<User>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted user id=${id}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  /** POST: add a new sensor */
  addSensor(sensor: any): Observable<Sensor> {
    return this.http.post<Sensor>(this.sensorsApi, sensor, httpOptions ).pipe(
      tap((s: Sensor) => this.log(`added sensor with id=${s._id}`)),
      catchError(this.handleError<Sensor>('addUser'))
    );
  }

  /** DELETE: delete sensor */
  deleteSensor(id:  string): Observable<Sensor> {
    const url = `${this.sensorsApi}/${id}`;

    return this.http.delete<Sensor>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted sensor id=${id}`)),
      catchError(this.handleError<any>('deleteSensor'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a UserService message with the MessageService */
  private log(message: string) {
    this.messageService.add('DataService: ' + message);
  }
}
