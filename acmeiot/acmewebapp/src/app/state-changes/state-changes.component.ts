import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User, Notification } from '../data.model';
import { DataService } from '../data.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-state-changes',
  templateUrl: './state-changes.component.html',
  styleUrls: ['./state-changes.component.css']
})
export class StateChangesComponent implements OnInit {
  notifications: Notification[];
  userId: string;
  sensorId: string;
  userName: string;

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private location: Location) { }

  ngOnInit() {
    this.userId = this.route.snapshot.paramMap.get('userid');
    this.sensorId = this.route.snapshot.paramMap.get('sensorid');

    this.getNotifications();
  }

  goBack(): void {
    this.location.back();
  }

  getNotifications(): void {
    this.dataService.getUser(this.userId).subscribe((user: User) => {
      this.userName = user.name;
      this.dataService.getNotifications(this.userId, this.sensorId)
      .subscribe( (d: Notification[]) => this.notifications = d);
    });

  }

}
