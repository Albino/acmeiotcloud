export class User {
  _id: string;
  name: string;
}

export class Sensor {
  _id: string;
  name: string;
  userId: string;
}

export class Notification {
  _id: string;
  userId: string;
  sensorId: string;
  sensorName: string;
  currentValue: number;
  previousValue: number;
  measurementTime: string;
}
