import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent } from './users/users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { StateChangesComponent } from './state-changes/state-changes.component';

const routes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  { path: 'detail/:userid', component: UserDetailComponent },
  { path: 'notifications/:userid', component: StateChangesComponent},
  { path: 'notifications/:userid/:sensorid', component: StateChangesComponent},
  { path: 'users', component: UsersComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
