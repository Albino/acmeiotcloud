#!/bin/bash
if [ -d "./clientbuild" ]; then
    echo "clean up clientbuild"
    rm -R ./clientbuild
fi
cd ../acmewebapp
if [ -d "./dist" ]; then
    echo "clean up dist"
    rm -R ./dist
fi
echo "build client"
ng build
echo "move to publish folder (clientbuild)"
mv ./dist ../acmeiotserver/clientbuild
