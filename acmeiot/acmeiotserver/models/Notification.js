let mongoose = require("mongoose");
let Schema   = mongoose.Schema;
let ObjectId = Schema.ObjectId;
 
let schema = new Schema({
    sensorId: {
        type: ObjectId,
        ref: 'Sensor',
        required: true
    },
    sensorName: {
        type: String,
        required: true
    },
    previousValue: {
        type: Number,
        required: false,
    },
    currentValue: {
        type: Number,
        required: true
    },
    measurementTime: {
        type: Date,
        required: true
    }
});
mongoose.model('Notification', schema);