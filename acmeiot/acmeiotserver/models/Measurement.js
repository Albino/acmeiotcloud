let mongoose = require("mongoose");
let Schema   = mongoose.Schema;
let ObjectId = Schema.ObjectId;
 
let schema = new Schema({
    value: {
        type: Number,
        required: true
    },
    sensorId: {
        type: ObjectId,
        ref: 'Sensor',
        required: true
    },
    measurementTime: {
        type: Date,
        required: true
    }
});

mongoose.model('Measurement', schema);