let mongoose = require("mongoose");
let Schema   = mongoose.Schema;
let ObjectId = Schema.ObjectId;
 
let schema = new Schema({
    name: {
        type: String,
        required: true
    },
    userId: {
        type: ObjectId,
        ref:'User',
        required: true
    }
});
mongoose.model('Sensor', schema);