let mongoose = require('mongoose'),
    fs = require('fs'),
    models_path = process.cwd() + '/models';

let mongoUrl = process.env.MONGO_URL || "mongodb://127.0.0.1:27017/acmedb";

console.log("Models path: " + models_path);    
console.log("Mongo connection string:" + mongoUrl);

mongoose.connect(mongoUrl, {server:{auto_reconnect:true}});
let db = mongoose.connection;

db.on('error', function (err) {
    console.error('MongoDB connection error:', err);
});

db.once('open', function callback() {
    console.info('MongoDB connection is established');
});

db.on('disconnected', function() {
    console.error('MongoDB disconnected!');
    mongoose.connect(mongoUrl, {server:{auto_reconnect:true}});
});

db.on('reconnected', function () {
    console.info('MongoDB reconnected!');
});

fs.readdirSync(models_path).forEach(function (file) {
    if (~file.indexOf('.js')){
        console.log("Loading model: "+models_path+'/'+file);
        require(models_path + '/' + file);
    }
});