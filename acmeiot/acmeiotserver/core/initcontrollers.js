let restify = require('restify'),
    errors = require('restify-errors'),
    restifyMongoose = require('restify-mongoose'),
    mongoose = require('mongoose'),
    corsMiddleware = require('restify-cors-middleware'),
    fs = require('fs');

// middleware
let server = restify.createServer();

let cors = corsMiddleware({
    origins: ['*']
});

server.pre(cors.preflight);
server.use(cors.actual);
server.use(restify.plugins.queryParser());
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.fullResponse());
server.use(restify.plugins.bodyParser());

// define endpoints
let userModel = mongoose.model("User");
let user = restifyMongoose(userModel);
let sensorModel = mongoose.model("Sensor");
let sensor = restifyMongoose(sensorModel);
let measurementModel = mongoose.model("Measurement");
let measurement = restifyMongoose(measurementModel);
let notificationModel = mongoose.model("Notification");
let notification = restifyMongoose(notificationModel);

server.get('/api/users', user.query());
server.get('/api/users/:id', user.detail());
server.post('/api/users', user.insert());
server.del('/api/users/:id', user.remove());

server.get('/api/sensors', sensor.query());
server.get('/api/sensors/:id', sensor.detail());
server.post('/api/sensors', sensor.insert());
server.del('/api/sensors/:id', sensor.remove());

server.post('/api/measurements', handleMeasurement);

server.get('/api/notifications', notification.query());

server.get('/*' , restify.plugins.serveStatic({
    directory: './clientbuild',
    default: 'index.html',
    appendRequestPath: false,
    maxAge: 0     
}));

// start listening
let port = process.env.PORT || 3330;
server.listen(port, function (err) {
    if (err)
        console.error(err);
    else
        console.log('ACME Web API is listening at : ' + port);
});

console.log("server listening");
if (process.env.environment == 'production'){
    process.on('uncaughtException', function (err) {
        console.error(JSON.parse(JSON.stringify(err, ['stack', 'message', 'inner'], 2)));
    });
}

// Measurement registering logic
function handleMeasurement(req, res, next){
    if (!req.is('application/json')) {
        return next(new errors.InvalidContentError("Expects 'application/json'"));
    }

    let data = req.body || {};
    let msrmnt = new measurementModel(data);

    //Get previous measurement for that sensor
    let previousSensorState;
    measurementModel.findOne( {$query: {sensorId: msrmnt.sensorId }, $orderby: {measurementTime: -1}}).populate("sensorId", "name").exec( function(err, previousMsrmnt){
        if(err){
            console.error("Retrieve measurement: " + err);
            return next(new errors.InternalError(err.message));
            next();
        } else {
    // Notify user of sensor state change
    // In a real system push notifications would be managed by a proper push notification server
    // and instead of a database write a message would be sent to the notification server.
            if( previousMsrmnt ){
                if(previousMsrmnt.value !== msrmnt.value){
                    notificationModel.create({
                        sensorId: msrmnt.sensorId,
                        sensorName: previousMsrmnt.sensorId.name,
                        previousValue: previousMsrmnt.value,
                        currentValue: msrmnt.value,
                        measurementTime: msrmnt.measurementTime
                    });
                }
            }
        }

        // save measurement
        msrmnt.save(function(err) {
            if (err) {
                console.error(err);
                return next(new errors.InternalError(err.message));
                next();
            }
            res.send(201);
            next();
        }); 
    });
}