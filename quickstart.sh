#!/bin/bash

docker pull mongo

cd acmeiot
docker build -t acmeiotcloud/acmeiotsolution .
cd ..

cd sensormocker
docker build -t acmeiotcloud/sensormocker .
cd ..

docker-compose up -d
