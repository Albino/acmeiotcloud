let mongoose   = require("mongoose"),
    request    = require("request");

let Schema   = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let mongoUrl = process.env.MONGO_URL || "mongodb://127.0.0.1:27017/acmedb";

mongoose.connect(mongoUrl);
let db = mongoose.connection;

db.on('error', function (err) {
    console.error('MongoDB connection error:', err);
});

db.once('open', function callback() {
    console.info('MongoDB connection is established');
});

db.on('disconnected', function() {
    console.error('MongoDB disconnected!');
});

let sensorSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    userId: {
        type: ObjectId,
        ref:'User',
        required: true
    }
});
let Sensor = mongoose.model('Sensor', sensorSchema);

let measurementApi = process.env.ACMEIOTSERVER_URL || "http://acmeiotsolution:3330";
measurementApi = measurementApi + "/api/measurements";

Sensor.find(emulateSensors);

function emulateSensors(err, sensors){
    if(err){
        console.log("Could not retrieve sensors from db: " + err);
    } else {
        sensors.forEach(sensor => {

            console.log("Handling sensor: " + sensor.name);

            if(Math.random() > 0.2){
                console.log("Sensor produced data");
                
                let newMeasurement = {
                    value: Math.random()*100.,
                    sensorId: mongoose.Types.ObjectId(sensor._id),
                    measurementTime: Date.now()
                };

                let requestOptions = {
                    method: 'POST',
                    url: measurementApi,
                    body: newMeasurement,
                    json: true
                };

                console.log("Request: " + requestOptions);

                request( requestOptions, function(err, response, body){
                    if(err){
                        console.log("Error: could not create measurement: " + err);
                    }else{
                        console.log(response);
                        console.log(body);
                    }
                });

            } else {
                console.log(sensor.name + ": no data generated");
            }
        });
    }
}





