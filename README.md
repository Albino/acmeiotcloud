# ACME IOT Cloud Demo

## Quick start up
```bash
    $> ./quickstart.sh # Build docker images from sources and run docker-compose up -d
```
You can now point your browser to http://localhost:3330 and look around.

The solution has a few components, bellow:
## Database
For sake of time, a single mongo database will be shared among the components. 
## Sensor Mocker
A simple sensor mock service. Implemented as a cron job. Periodically generates sensor mock data and call the sensor update api in the acme server.

## Acme Web App
Angular app for user interaction with the data

## IOT Server
A nodejs/restify server that provides API for registering sensor state updates and generating push notifications in case of state changes. Exposes an API for populating the web app interface as well.

![Architecture Overview](images/architecture2.png)